import time
import random


def bubble_sort(array):
    for i in range(len(array)):
        for j in range(len(array) - 1):
            if array[j] > array[j + 1]:
                array[j], array[j + 1] = array[j + 1], array[j]

    return array

arr_100  = [random.randint(0, 100) for i in range(100)]
arr_1000 = [random.randint(0, 100) for i in range(1000)]
arr_10000 = [random.randint(0, 100) for i in range(10000)]

start = time.time()

bubble_sort(arr_100)
end = time.time()

print("100 elements: ", end - start)

start = time.time()

bubble_sort(arr_1000)
end = time.time()

print("1000 elements: ", end - start)

start = time.time()

bubble_sort(arr_10000)
end = time.time()

print("10000 elements: ", end - start)

