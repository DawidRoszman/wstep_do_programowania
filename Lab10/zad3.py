with open('liczby.txt', 'w') as f:
    for i in range(100):
        f.write(str(i) + '\n')

new_lines: list[str] = []

with open('liczby.txt', 'r') as f:
    for line in f:
        new_lines.append(line.strip())

print(*new_lines, sep='\n')

with open("liczby.txt", "w") as f:
    for new_line in new_lines:
        n = int(new_line)
        if n % 2 == 0:
            n += 10
        f.write(str(n) + "\n")
