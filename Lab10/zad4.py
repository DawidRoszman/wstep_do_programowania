import pandas as pd

# Wczytanie pliku do obiektu DataFrame
df = pd.read_csv('miasta.csv')

# Dodanie nowego wiersza do tabeli
df = df.append({'Rok': 2010, 'Londyn': 460, 'Nowy Jork': 555, 'Hong Kong': 405}, ignore_index=True)

# Zapisanie tabeli do pliku
df.to_csv('miasta.csv', index=False)
