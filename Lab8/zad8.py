"""
n
jesli n = 0 zwracamy 1
print n%2
zwracamy n//2
"""
def to_binary(n):
    if n > 1:
         to_binary(n//2)
    print(n%2, end="")

def binary_to_decimal(n):
    if n == 0:
        return 0
    return n%10 + 2*binary_to_decimal(n//10)

def main():
    to_binary(6)
    print()
    print(binary_to_decimal(110))

if __name__=="__main__":
    main()

