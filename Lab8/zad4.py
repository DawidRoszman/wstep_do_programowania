def suma(n):
    if n == 0:
        return n
    return suma(n-1)+n


def main():
    print(suma(4))


if __name__ == "__main__":
    main()
