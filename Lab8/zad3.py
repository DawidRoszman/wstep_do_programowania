def main(n):
    out = 1
    for i in range(n):
        out *= i+1
    print(out)


if __name__ == "__main__":
    main(3)
