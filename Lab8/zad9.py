def factorize(n):
    if n <= 1:
        return
    for i in range(2,n+1):
        if not is_prime(i):
            continue
        if n%i==0:
            print(i, end=" ")
            factorize(n//i)
            break

def is_prime(n):
    if n>1:
        for i in range(2, int(n/2)+1):
            if n%i == 0:
                return False
    return True

def main():
    factorize(90)

if __name__=="__main__":
    main()
