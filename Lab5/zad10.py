x = 156

# # change x to binary number
# x = bin(x)
# print(str(x)[2:])
c = []
while x > 0:
    c.append(0 if x%2 == 0 else 1)
    x //= 2
print(*c[::-1])