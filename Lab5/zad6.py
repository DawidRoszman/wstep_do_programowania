def checkIfPrimary(x):
    if x == 0 or x == 1:
        return False
    for i in range(x):
        if i == 1 or i ==0:
            continue
        if x%i==0:
            return False
    return True

x = int(input("Podaj liczbe"))
i = 1
li = []
while i < x:
    if checkIfPrimary(i):
        li.append(i)
    i+=1
print(li, len(li))