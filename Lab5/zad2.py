a = [[1,2,3],[4,5,6]]
b = [[10,11],[20,21],[30,31]]

#multiply matrices a and b
c = [[0,0],[0,0]]
for i in range(len(a)):
    for j in range(len(b[0])):
        for k in range(len(b)):
            c[i][j] += a[i][k] * b[k][j]

print(*[x for x in c], sep="\n")