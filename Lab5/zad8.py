from random import randint
to_guess = randint(1,100)
i = 1
while True:
    num = int(input("Podaj liczbe od 1 do 100: "))
    if num == to_guess:
        print(f"Congrats you won on {i} try")
        break
    else:
        print(f"Sorry, {i} try")
        if to_guess > num:
            print("Your number is too low")
        elif to_guess < num:
            print("Your number is too high")
        i+=1