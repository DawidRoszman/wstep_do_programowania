import sys
from math import sqrt
a,b,c = sys.argv[1:4]
a = int(a)
b = int(b)
c = int(c)
delta = b**2-4*a*c
if a == b == c == 0:
    print("Nieskonczenie wiele rozwiazan")
elif a == 0:
    print("ma jedno rozwiazanie:", -b/c)
elif delta < 0:
    print("brak rozwiazan")
elif delta == 0:
    print("jedno rozwiazanie:", -b/2*a)
else:
    x1 = (-b-sqrt(delta))/(2*a)
    x2 = (-b+sqrt(delta))/(2*a)
    print(f'dwa rozwiazania {x1} i {x2}')
