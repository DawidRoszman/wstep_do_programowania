
def dodaj_laptop(lista_laptopow: list, nazwa: str, procesor: str, karta_graficzna: str, ram_karty: float, ram: float, pamiec: float, ekran_w_calach: float, bateria: float, cena: float) -> list:
    """Add laptop to list of laptops

    Args:
        lista_laptopow (list)
        nazwa (str)
        procesor (str) 
        karta_graficzna (str)
        ram_karty (float)
        ram (float)
        pamiec (float)
        ekran_w_calach (float)
        bateria (float)
        cena (float)

    Returns:
        list: New list with added laptop 
    """
    t = lista_laptopow.copy()
    t.append(
        {'nazwa': nazwa, 'procesor': procesor, 'karta_graficzna': {'nazwa': karta_graficzna, 'ram_karty': ram_karty}, 'ram': ram,
            'pamiec': pamiec, 'ekran_w_calach': ekran_w_calach, 'bateria': bateria, 'cena': cena}
    )
    return t


laptopy = []

laptopy = dodaj_laptop(laptopy, 'HP Pavilon Gaming', 'AMD Ryzen 7 5800H',
                       'Nvidia GeForce RTX 3050', 4, 16, 512, 15.6, 96, 4499)
laptopy = dodaj_laptop(laptopy, 'Apple MacBook Air',
                       'M1', 'M1', 4, 16, 256, 13.3, 96, 5051.38)

print(*laptopy)
