import math


def find_hypnotenuse(cathetus1, cathetus2):
    hypnotenuse = math.sqrt(cathetus1**2+cathetus2**2)
    return hypnotenuse


def main():
    a = 3
    b = 4
    c = find_hypnotenuse(a, b)
    print(a, b, c)


if __name__ == "__main__":
    main()
