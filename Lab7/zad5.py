from zad6 import odd_sum_dict
from zad7 import sum_dictionaries


def merge_dicts(dict1: dict, dict2: dict) -> dict:
    return {**dict1, **dict2}


def main():
    dict1 = {1: 10, 2: 7, 3: 8, 4: 15, 5: 14, 6: 3, 7: 2}
    dict2 = {8: 10, 9: 40, 10: 34, 11: 100}
    dict3 = {8: 10, 9: 40, 1: 55}
    print(merge_dicts(dict1, dict3))
    print(odd_sum_dict(dict1))
    dict_a = {1: 10, 2: 20, 3: 30, 4: 40}
    dict_b = {1: 11, 3: 33, 5: 55}
    print(sum_dictionaries(dict_a, dict_b))


if __name__ == "__main__":
    main()
