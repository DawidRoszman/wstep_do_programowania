def sum_dictionaries(dict1, dict2):
    out = {}
    for i in dict1:
        if i in dict2:
            out[i] = dict1[i] + dict2[i]
    return out
