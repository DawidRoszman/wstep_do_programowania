towar = [{'nazwa': 'banan', 'jednostka': 'kg', 'ilosc': 10, 'cena': 3},
         {'nazwa': 'jabłko', 'jednostka': 'kg', 'ilosc': 16, 'cena': 2.5},
         {'nazwa': 'mąka pszenna', 'jednostka': 'op.', 'ilosc': 30, 'cena': 2.5},
         {'nazwa': 'mydło', 'jednostka': 'szt.', 'ilosc': 6, 'cena': 1.5},
         {'nazwa': 'jogurt naturalny', 'jednostka': 'szt.', 'ilosc': 20, 'cena': 1.5},
         {'nazwa': 'papier toaletowy 8 rolek', 'jednostka': 'op.', 'ilosc': 10, 'cena': 9}]


def wyszukaj(towar: list, nazwa: str) -> dict:
    for t in towar:
        if t['nazwa'] == nazwa:
            return t


def sumuj(towar: list, nazwa: str) -> float:
    for t in towar:
        if t['nazwa'] == nazwa:
            return t['ilosc']*t['cena']


def sumuj_wszystko(towar: list) -> float:
    suma = 0
    for t in towar:
        suma += t['ilosc']*t['cena']
    return suma


def dodaj_towar(towar: list, nazwa: str, jednostka: str, ilosc: int, cena: float) -> list:
    t = towar.copy()
    t.append(
        {'nazwa': nazwa, 'jednostka': jednostka, 'ilosc': ilosc, 'cena': cena}
    )
    return t


def aktualizuj_ilosc(towar: list, nazwa: str, ilosc: int) -> list:
    t = towar.copy()
    for i in t:
        if i['nazwa'] == nazwa:
            i['ilosc'] = ilosc
    return t


def filtr_jednostka(towar: list, jednostka: str) -> list:
    out = []
    for i in towar:
        if i['jednostka'] == jednostka:
            out.append(i)
    return out


def main():
    print(wyszukaj(towar, 'banan'))
    print()
    print(sumuj(towar, 'jogurt naturalny'))
    print()
    print(sumuj_wszystko(towar))
    print()
    print(dodaj_towar(towar, 'gruszka', 'kg', 10, 2.99))
    print()
    print(aktualizuj_ilosc(towar, 'banan', 24))
    print()
    print(filtr_jednostka(towar, 'szt.'))


if __name__ == "__main__":
    main()
