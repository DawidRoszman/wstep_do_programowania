def odd_sum_dict(dict: dict) -> float:
    sum = 0
    for i in dict:
        if dict[i] % 2 != 0:
            sum += dict[i]
    return sum
