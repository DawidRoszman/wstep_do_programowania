import math


def find_hypnotenuse(cathetus1: float, cathetus2: float) -> float:
    """the function computes a hypotenuse of
    of a trianble based on the given two catheti

    Args:
        cathetus1 (float): cathetus 1
        cathetus2 (float): cathetus 2

    Returns:
        float: hypnotenuse
    """
    hypnotenuse = math.sqrt(cathetus1**2+cathetus2**2)
    return hypnotenuse


def find_angles(cathetus1: float, cathetus2: float) -> tuple:
    """finds angles

    Args:
        cathetus1 (float): cathetus 1
        cathetus2 (float): cathetus 2

    Returns:
        tuple(float,float): angles in degrees
    """
    angle = math.degrees(math.atan(cathetus1/cathetus2))
    return (angle, 90-angle),


def main():
    a = 3
    b = 3
    c = find_hypnotenuse(a, b)
    print(a, b, c)
    print(find_angles(a, b))


if __name__ == "__main__":
    main()
