import math


def find_hypnotenuse(cathetus1, cathetus2):
    """the function computes a hypotenuse of
    of a trianble based on the given two catheti

    Args:
        cathetus1 (float): cathetus 1
        cathetus2 (float): cathetus 2

    Returns:
        float: hypnotenuse
    """
    hypnotenuse = math.sqrt(cathetus1**2+cathetus2**2)
    return hypnotenuse


def main():
    a = 3
    b = 4
    c = find_hypnotenuse(a, b)
    print(a, b, c)


if __name__ == "__main__":
    main()
