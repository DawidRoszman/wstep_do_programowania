a,b = input("Podaj boki prostokata a i b:").split(" ")
a,b = int(a), int(b)
if a <= 0 or b <= 0:
    print("Nieprawidlowe dane")
else:
    print(f"Pole prostokata P={a*b}, a obwod L={a*2+b*2}")
