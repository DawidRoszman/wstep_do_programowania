from random import randint
n,m = 3,3

a = []
for i in range(n):
    a.append([])
    for j in range(m):
        a[i].append(randint(0,20))
        print(f"{a[i][j] : 5}", end=" ")
    print("\n")

print(f"Suma {sum([a[x][x] for x in range(n)])}")
