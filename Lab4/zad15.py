from random import randint, seed
# seed(321731)


n = 7
m = 7

a = [[]*n]
for i in range(n):
    a.append([])
    for j in range(m):
        a[i].append(randint(0,20))

for i in range(n):
    for j in range(m):
        print(f"{a[i][j] : 5}", end=" ")
    print("\n")

for_sum = []
for i in range(n):
    for j in range(m):
        if j > i:
            for_sum.append(a[i][j])

print(f"Suma {sum(for_sum)}")