def nwd_rekurencja(a, b):
    if b > a:
        return nwd_rekurencja(b, a)

    if a % b == 0:
        return b

    return nwd_rekurencja(b, a % b)   

def nwd(a, b):
  while a != b:
      if(a>b):
          a-=b
      else:
          b-=a
  return a
