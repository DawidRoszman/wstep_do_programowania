res = []


def permute(a, l, r):
    global res
    a_copy = a[:]
    if l == r:
        s = a_copy
        res.append(s)
    for i in range(l, r+1):
        a_copy[l], a_copy[i] = a_copy[i], a_copy[l]
        permute(a_copy, l+1, r)
        a_copy[l], a_copy[i] = a_copy[i], a_copy[l]


a = ['A', 'B', 'C']
result = permute(a, 0, len(a)-1)

print(res)
