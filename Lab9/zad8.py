def permutation(lst):
    if len(lst) == 0:
        return []
    if len(lst) == 1:
        return [lst]
    perms = []
    for i in range(len(lst)):
        m = lst[i]
        remLst = lst[:i] + lst[i+1:]
        for p in permutation(remLst):
            perms.append([m] + p)
    return perms


print(permutation(['A', 'B', 'C']))

# c) Te linikji złużą do przerwania wykonywania rekurencji gdy lista nie ma elementów lub gdy lista ma tylko jeden element
# d) w lst jest lista, która jest przekazywana do funkcji rekurencyjnej, a perms jest listą, która przechowuje wszystkie permutacje
# e) do m jest przypisywany element z listy, a remLst jest listą, która zawiera wszystkie elementy oprócz tego, który jest przypisany do m
# f) podaj przykład lst, i, m i remLst: lst = [1, 2, 3], i = 1, m = 2, remLst = [1, 3]
# g) do czego służy pętla for w linijsce 11: służy do wywołania funkcji rekurencyjnej dla każdego elementu z listy.
