from zad9 import odleglosc


def naJednejLini(P1, P2, P3):
    AB = odleglosc(P1, P2)
    AC = odleglosc(P1, P3)
    BC = odleglosc(P2, P3)
    if AB == AC-BC or AB == abs(AC-BC):
        return True
    return False


print(naJednejLini((0, 0), (1, 1), (2, 2)))
