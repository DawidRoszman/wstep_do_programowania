from math import sqrt


def odleglosc(P1, P2):
    a = P2[0]-P1[0]
    b = P2[1]-P1[1]
    return sqrt((a)**2+(b)**2)
