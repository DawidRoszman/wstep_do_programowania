from math import sqrt


def odleglosc(P1, P2):
    a = P2[0]-P1[0]
    b = P2[1]-P1[1]
    return sqrt((a)**2+(b)**2)


def obwodTrojkota(a, b, c):
    odl_a_b = odleglosc(a, b)
    odl_a_c = odleglosc(a, c)
    odl_c_b = odleglosc(c, b)
    print(odl_a_b+odl_a_c+odl_c_b)


if __name__ == "__main__":
    print(obwodTrojkota([7.5, -8], [-2, 0], [7, 12]))
    print(obwodTrojkota([0, 0], [0, 4], [3, 0]))
