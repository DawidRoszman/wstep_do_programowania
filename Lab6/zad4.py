def powitaj(imie):
    print("Czesc", imie, "!")


def sumuj(a, b):
    print("Suma", a, "i", b, "to", (a+b))


powitaj()
sumuj(3)
sumuj()
sumuj(b=9)
sumuj(a=9)
sumuj(b=4, a=3)
